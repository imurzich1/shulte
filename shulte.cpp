#include "shulte.h"
#include <random>

Shulte::Shulte(QObject *parent)
    : QObject{parent}
{

}

bool Shulte::checkNumber(int num)
{
    if (num == next_num)
    {
        current_num++;
        next_num++;
        completed++;
        return true;
    } else {
        return false;
    }
}

void Shulte::newGame(int table_size){
    this->table_size = table_size;
    this->total_num = table_size * table_size;
    numbers.clear();
    numbers.resize(table_size * table_size);
    std::iota(numbers.begin(), numbers.end(), 1);
    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle(numbers.begin(), numbers.end(), g);
}
