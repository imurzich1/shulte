#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include <QDebug>
#include <QProgressBar>
#include "shulteview.h"
#include "shulte.h"
#include <iostream>
#include <ostream>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    shulte = new Shulte(this);
    shulte_view = new ShulteView(this, shulte);
    setCentralWidget(ui->gridLayoutWidget);
    ui->gridLayout->addWidget(shulte_view);
    progress = new QProgressBar(this);
    progress->setMinimum(0);
    ui->statusbar->addWidget(progress);
    connect(shulte_view, SIGNAL(completedNum(int)), progress, SLOT(setValue(int)));

    this->newGame(5);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete shulte_view;
    delete shulte;
    delete progress;
}

void MainWindow::newGame(int num)
{
    this->shulte->newGame(num);
    this->shulte_view->refresh();
    this->progress->reset();
    this->progress->setMaximum(shulte->total_num);
}

void MainWindow::setTable5()
{
    this->newGame(5);
}
void MainWindow::setTable6()
{
    this->newGame(6);
}
void MainWindow::setTable7()
{
    this->newGame(7);
}
void MainWindow::setTable8()
{
    this->newGame(8);
}
void MainWindow::setTable9()
{
    this->newGame(9);
}
void MainWindow::setTable10()
{
    this->newGame(10);
}
