#include "shulte.h"
#include "shulteview.h"
#include <QResizeEvent>
#include <QTableWidget>
#include <QHeaderView>
#include <QDebug>
#include <QRandomGenerator>

ShulteView::ShulteView(QWidget *parent, Shulte *shulte)
    : QTableWidget{parent}
{
    this->shulte = shulte;
    this->horizontalHeader()->hide();
    this->verticalHeader()->hide();
    this->setSelectionMode(SingleSelection);
    connect( this, SIGNAL( cellClicked(int,int)),
            this, SLOT( cellSelected(int,int)));
};

void ShulteView::refresh()
{
    this->setRowCount(shulte->table_size);
    this->setColumnCount(shulte->table_size);
    std::vector<int>::iterator it = shulte->numbers.begin();
    for (int c = 0; c < shulte->table_size; ++c) {
        for (int r = 0; r < shulte->table_size; ++r) {
            int n = *it;
            it++;
            QString s = QString::number(n);
            QTableWidgetItem * item = new QTableWidgetItem(s);
            item->setTextAlignment(Qt::AlignLeft + Qt::AlignCenter);
            item->setFlags(item->flags() & ~Qt::ItemIsEditable);
            this->setItem(r, c, item);
        }
    }
    this->resize(this->size());
}

void ShulteView::resizeEvent ( QResizeEvent * event )
{
    this->resize(event->size());
};


void ShulteView::resize ( QSize size )
{
    int width = size.width() / shulte->table_size;
    for (int c = 0; c < shulte->table_size; ++c) {
        this->setColumnWidth(c, width);
    }
    int height = size.height() / shulte->table_size;
    for (int r = 0; r < shulte->table_size; ++r) {
        this->setRowHeight(r, height);
    }
    int font_size = 0;
    if (height > width) {
        font_size = width / 4;
    } else {
        font_size = height / 4;
    }
    for (int c = 0; c < shulte->table_size; ++c) {
        for (int r = 0; r < shulte->table_size; ++r) {
            QFont font = this->font();
            font.setPixelSize(font_size);
            this->setFont(font);
        }
    }
};


void ShulteView::cellSelected(QTableWidgetItem *item)
{
    qDebug() << "cellSelected";

}

void ShulteView::cellSelected(int nRow, int nCol)
{
    for (int r = 0; r < shulte->table_size; ++r) {
        for (int c = 0; c < shulte->table_size; ++c) {
            this->item(r, c)->setBackground(QBrush(Qt::white));
        }
    }
    QTableWidgetItem *item = this->item(nRow, nCol);
    item->setSelected(false);
    int num = item->text().toInt();
    if (shulte->checkNumber(num)) {
        item->setBackground(QBrush(Qt::green));
        emit completedNum(shulte->completed);
    } else {
        item->setBackground(QBrush(Qt::red));
    }
}
