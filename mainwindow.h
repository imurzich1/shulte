#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "QLabel"
#include "qprogressbar.h"
#include "shulteview.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QProgressBar *progress;
    ShulteView *shulte_view;
    Shulte *shulte;
    void newGame(int num);

public slots:
    void setTable5();
    void setTable6();
    void setTable7();
    void setTable8();
    void setTable9();
    void setTable10();

};
#endif // MAINWINDOW_H
