#ifndef SHULTE_H
#define SHULTE_H

#include <QObject>

class Shulte : public QObject
{
    Q_OBJECT
public:
    explicit Shulte(QObject *parent = nullptr);
    void newGame(int table_size)    ;

    int table_size;
    int current_num = 0;
    int total_num = 0;
    bool checkNumber(int num);
    int completed = 0;
    std::vector<int> numbers;

private:
    int next_num = 1;

signals:
    void correctNumber(int num);
    void completeTable();
    void completedNum(int num);
};

#endif // SHULTE_H
