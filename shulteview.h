#ifndef SHULTEVIEW_H
#define SHULTEVIEW_H

#include "qtablewidget.h"
#include "shulte.h"
#include <QWidget>

class ShulteView : public QTableWidget
{
    Q_OBJECT

public:
    explicit ShulteView(QWidget *parent = nullptr, Shulte *shulte = nullptr);
    void refresh();

protected:

    void resizeEvent ( QResizeEvent * event );

private:
    void resize(QSize size);
    Shulte *shulte;

public slots:
    void cellSelected(QTableWidgetItem *item);
    void cellSelected(int nRow, int nCol);

signals:
    void numberChanged(int num);
    void completedNum(int num);

};

#endif // SHULTEVIEW_H
